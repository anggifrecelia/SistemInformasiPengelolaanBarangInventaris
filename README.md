-------------Sistem Informasi Pengelolaan Barang Inventaris Inventaris Institut Teknologi Del---------

Topik		: Sistem Informasi Pengelolaan Barang Inventaris Institut Teknologi Del
Kelompok	: PSI 12
Anggota		: 1. Arsantya Saragih (12S15020)
		  2. Riahta Mei (12S15038)
		  3. Widya Simbolon (12S15042)
		  4. Anggi Frecellia (12S15048)

Pembimbing	: Samuel I.G. Situmeang S.Ti., M.Sc.
Penguji 1	: Mario Simare-mare S.Kom., M.Sc.

Seminar
Hari/Tanggal	: Rabu, 06 Juni 2018
Pukul		: 17.30-19.00 WIB
Tempat		: GD516


Install Sistem Informasi Pengelolaan Barang Inventaris Institut Teknologi Del:

Langkah-langkah instalasi:
*catatan: Semua langkah ini dilakukan berdasarkan spesifikasi yang digunakan developer pada Windows 8.1 64bit
atau Windows 7 64bit. Di luar OS tersebut, belum ada percobaan.

Pre-Instalation:
- XAMPP
- Database yang sudah dibuat

1.Pindahkan aplikasi dengan folder "inventory" ke:
C:/xampp/htdocs/inventory

2.Buka folder:
C:/xampp/htdocs/inventory/common/config

3.Buka file: "main-local.php"

4.Tambahkan environment variable computer anda ke folder "inventory"

5.Jalankan pada command prompt:
	"yii request"

6.Jalankan aplikasi dengan browser:
"http://localhost/inventory/backend/web/index.php" untuk user


Create Database:
Langkah-langkah:
1. Buka Browser:
	"http://localhost/phpmyadmin/"
2. Click New
	Buat nama: "inventory"
3. Pilih menu "Import". Browse database: inventory.sql
4. Click Go


Petunjuk Penggunaan Aplikasi :

1. Login user sebagai Mahasiswa atau Dosen/Staf.

username : anggi
password : anggi123

- Jika ingin melakukan peminjaman barang aset, maka user memilih menu Peminjaman, menekan tombol "Buat Peminjaman Baru".
Kemudian mengisi form Peminjaman sesuai kebutuhan user. Setelah itu menekan tombol "Kirim". Permintaan peminjaman
user akan dikirimkan kepada Pihak Inventory untuk dikonfirmasi persetujuannya.

Tanggal Peminjaman dapat dilakukan 2 hari setelah tanggal transaksi peminjaman dan waktu peminjaman user maksimal 
hanya 5 hari.

Jika Pihak Inventory menyetujuinya, maka user harus memilih tombol "Detail Peminjaman" kemudian mencetak bukti peminjaman
dengan memilih tombol "Cetak Bukti Peminjaman" yang akan digunakan sebagai bukti pengambilan barang aset.

- Jika ingin melakukan permintaan barang habis pakai, maka user memilih menu Permintaan, menekan tombol "Buat Permintaan Baru".
Kemudian mengisi form Permintaan sesuai kebutuhan user. Setelah itu menekan tombol "Kirim". Permintaan barang habis pakai
user akan dikirimkan kepada Pihak Inventory untuk dikonfirmasi persetujuannya.

Jika Pihak Inventory menyetujuinya, maka user harus memilih tombol "Detail Permintaan" kemudian mencetak bukti permintaan
dengan memilih tombol "Cetak Bukti Permintaan" yang akan digunakan sebagai bukti permintaan barang habis pakai.

2. Login user sebagai Pihak Inventory 

username : suci
password : meii12345

- Jika ingin mengelola Data Master Barang, memilih menu Data Master Barang, memilih tombol "Daftarkan Barang Baru".
Kemudian mengisi form pendaftaran barang baru sesuai kebutuhan. Untuk mengubah data barang, user hanya menekan tombol edit
yang disimbolkan dengan icon "pulpen" di samping data barang. Begitu juga untuk menghapus, dengan menekan icon "tong sampah",
dan untuk melihat detail barang dengan menekan tombol dengan icon "mata" disamping data barang.

User juga dapat mencari data barang dengan memilih kategori dan jenis barang kemudian menekan tombol "Search".

- Jika ingin mengelola Data Master Status, memilih menu Data Master Status, memilih tombol "Daftarkan Status Baru".
Kemudian mengisi form pendaftaran status baru sesuai kebutuhan. Untuk mengubah data status, user hanya menekan tombol edit
yang disimbolkan dengan icon "pulpen" di samping data status. Begitu juga untuk menghapus, dengan menekan icon "tong sampah",
dan untuk melihat detail status dengan menekan tombol dengan icon "mata" disamping data status.

- Jika ingin mengelola Data Master Gedung, memilih menu Data Master Gedung, memilih tombol "Daftarkan Gedung Baru".
Kemudian mengisi form pendaftaran gedung baru sesuai kebutuhan. Untuk mengubah data gedung, user hanya menekan tombol edit
yang disimbolkan dengan icon "pulpen" di samping data gedung. Begitu juga untuk menghapus, dengan menekan icon "tong sampah",
dan untuk melihat detail gedung dengan menekan tombol dengan icon "mata" disamping data gedung.

User juga dapat mencari data gedung dengan memilih tipe dan kelompok gedung kemudian menekan tombol "Search".

- Jika ingin melakukan penyetujuan peminjaman barang aset, maka memilih menu Peminjaman -> menekan tombol Approve pada permintaan peminjaman
barang aset yang dilakukan oleh pemohon. Untuk menolak, user hanya menekan tombol reject kemudian akan diarahkan ke halaman alasan penolakan
yang akan dikirimkan kepada pemohon.

- Jika ingin melakukan penyetujuan permintaan barang habis pakai, maka memilih menu Permintaan -> menekan tombol Approve pada permintaan barang
habis pakai yang dilakukan oleh pemohon. Untuk menolak, user hanya menekan tombol reject kemudian akan diarahkan ke halaman alasan penolakan
yang akan dikirimkan kepada pemohon.

- Jika ingin mengelola barang habis pakai,
+ Menambahkan barang habis pakai  ke dalam stok Inventory.
	- Memilih menu Data Barang Habis Pakai, menekan tombol " Daftarkan Barang Habis Pakai" dan mengisi form pendaftaran barang habis pakai sesuai keinginan user.

+ Melakukan pengadaan barang habis pakai.
	- Dari menu Data Barang Habis Pakai, memilih sub menu Pengadaan Barang. Kemudian mengisi form pengadaan barang sesuai kebutuhan user. Tanggal pengadaan > Tanggal Pendaftaran barang ke stok inventory.

- Jika ingin mengelola barang aset
+ Melakukan pendaftaran barang aset ke Buku Inventory. ( Tujuan : mengelompokkan daftar barang aset)

	- Memilih menu Data Barang Aset, memilih sub menu Buku Inventory, memilih tombol Daftarkan Barang Aset Baru, kemudian mengisi form pendaftaran aset sesuai kebutuhan.

+ Melakukan pencatatan aset ( Tujuan : mencatatkan penyebarang barang aset)

	- Memilih menu Data Barang Aset,memilih sub menu Pencatatan Aset, memilih tombol Daftarkan Barang Aset Baru, kemudian mengisi form pencatatan aset sesuai kebutuhan. Tanggal trans > Tanggal pendaftaran aset ke buku inventory.

+ Melakukan mutasi aset ( Tujuan : mencatatkan perpindahan aset dari satu lokasi ke lokasi lain )

	- Memilih menu Data Barang Aset, memilih sub menu Mutasi Aset, memilih tombol Daftarkan Mutasi Aset Baru, kemudian mengisi form mutasi aset sesuai kebutuhan. Tanggal trans > Tanggal trans pencatatan aset.

+ Melihat daftar kerusakan aset

	- Memilih menu Data Barang Aset, memilih sub menu Kerusakan, melihat daftar kerusakan aset yang dilaporkan oleh Pihak Inventory pada sub menu Buku Inventory.


- Jika ingin mencetak laporan Peminjaman Aset, memilih menu Laporan Peminjaman, melihat daftar peminjaman berdasarkan bulan dan tahun. Kemudian menekan tombol "Cetak Laporan".


- Jika ingin mencetak laporan Permintaan Barang Habis Pakai, memilih menu Laporan Permintaan, melihat daftar permintaan berdasarkan bulan dan tahun. Kemudian menekan tombol "Cetak Laporan".


- Jika ingin mencetak laporan Kerusakan, memilih menu Laporan Kerusakan, melihat daftar kerusakan berdasarkan bulan dan tahun. Kemudian menekan tombol "Cetak Laporan".


- Jika ingin mencetak laporan Mutasi Aset, memilih menu Laporan Mutasi Aset, melihat daftar mutasi aset berdasarkan bulan dan tahun. Kemudian menekan tombol "Cetak Laporan".

 
3. Login user sebagai Administrator

username : administrator
password : riahta123

Melihat role user :

- Memilih menu Role, melihat daftar User, jika ingin menghapus user, pilih tombol hapus disamping data user. Jika ingin mengubah data user, pilih tombol ubah dan jika
ingin melihat data user memilih tombol lihat di samping data user.

Menambahkan User :

- Memilih tombol Tambah User Baru kemudian mengisi form pendaftaran user baru dan menekan tombol tambahkan.



